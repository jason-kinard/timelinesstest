class SignupCardController {
  constructor($http, $window) {
    this.username = '';
    this.password = '';
    this.$http = $http;
    this.$window = $window;
  }

  submitForm() {
    let credentials = {
      username: this.username,
      password: this.password
    }
    this.$http
      .post('/api/users', credentials)
      .then(() => {
        this.$window.location.href = '/';
      })
      .catch(() => {
        this.error = 'Cannot create account with that username and password';
      });
  }
}

angular
  .module('app')
  .component('signUpCard', {
    templateUrl: '/app/sign-up-card/sign-up-card.component.html',
    controllerAs: 'signup',
    controller: [
      '$http',
      '$window',
      ($http, $window) => new SignupCardController($http, $window)
    ]
  });