angular
  .module('app')
  .config([
    '$stateProvider',
    '$urlServiceProvider',
    '$locationProvider',
    ($stateProvider, $urlServiceProvider, $locationProvider) => {
      const loginState = {
        name: 'login',
        url: '/session/new',
        component: 'login-page'
      };

      const signUpState = {
        name: 'signUp',
        url: '/users/new',
        component: 'sign-up-card'
      };

      $stateProvider.state(loginState);
      $stateProvider.state(signUpState);

      $locationProvider.html5Mode(true);
      $urlServiceProvider.rules.otherwise({ state: 'login' });
    }
  ]);